//
//  CFMobile.h
//  CFMobile SDK
//
//  Copyright (c) 2017 Cloudflare, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CFLogLevel) {
    CFLogLevelDetail  = 0x1,
    CFLogLevelWarning = 0x3,
    CFLogLevelError   = 0x4,
    CFLogLevelNone    = 0xF
};

@interface CFMobile : NSObject

/**
 Initialization is the process of modifying your application in order to communicate with Cloudflare network.
 Initialize CFMobile SDK on the main thread at beginning of your application delegate callback.
 ```
 [CFMobile initialize:clientKey];
 ```
 @param clientKey - application client key
 */
+ (void)initialize:(NSString *)clientKey;

/**
 Initialization is the process of modifying your application in order to communicate with Cloudflare network.
 Initialize CFMobile SDK on the main thread at beginning of your application delegate callback.
 ```
 [CFMobile initialize:clientKey completionHandler:^{
    if ([CFMobile initialized]) {
        //CFMobile SDK is ON.
    } else {
        //CFMobile SDK is OFF. Change log settings for more details.
        ...
    }
 }];
 ```
 @param clientKey - application client key
 @param completionHandler - a block is run after CFMobile SDK is asynchronously initialized.
                            The callback is executed on a background thread.
 */
+ (void)initialize:(NSString *)clientKey completionHandler:(void (^)(void))completionHandler;

/**
 Returns a boolean indicating CF Mobile SDK is enabled and ready to accelerate your network requests.
 
 @return boolean - true if enabled
*/
+ (BOOL)initialized;

/**
    Restarts routing requests to custom protocol handler if routing was explicitly stopped earlier.
 
    Note: Routing is automatically enabled when CFMobile SDK is initialized and this function should
    only be used if routing was explicitly disabled by calling `stopRouting` function.
 */
+ (void)restartRouting;

/**
    Stops routing requests to custom protocol handler.
 */
+ (void)stopRouting;

/**
 Return the current log level used by the SDK.
 
 @return NMLogLevel - logLevel
 */
+ (CFLogLevel)logLevel;

/**
    Sends the Key Performance Index (KPI) attributes to metrics endpoint.
 
    @param key
        NSString object representing KPI key.
    @param value
        NSString object representing KPI value.
 */
+ (void)reportKPIMetricWithKey:(NSString *)key andValue:(NSString *)value;

/**
 Set the log level used by the CF Mobile SDK.
 */
+ (void)setLogLevel:(CFLogLevel)logLevel;

@end
