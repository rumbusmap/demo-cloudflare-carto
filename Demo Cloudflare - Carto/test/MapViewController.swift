//
//  MapMapViewController.swift
//  test
//
//  Created by Rum Nguyen on 11/21/20.
//

import UIKit
import CartoMobileSDK

class MapViewController: UIViewController {

    lazy var label: UILabel = self.makeLabel()
    lazy var button: UIButton = self.makeButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let cfMobile = CFMobile.init()
        
        // 1. Config bMap
        // Contact BusMap for license key
        NTMapView.registerLicense("BN2x3YjFFQTYrVmZUZlJnVzR1RTRZS2FFaGJJdXZvR0VtdktLQ05wTktlTkxvWGdtb3VmNS9UT0FjOGpKL2p4WS9hSGcrcGs5eHFYbHdyMDBqNXoyQVE9PQoKYnVuZGxlSWRlbnRpZmllcj12bi5idXNtYXAuYm1hcC50ZXN0CnByb2R1Y3RzPXNkay1pb3MtNS4qCndhdGVybWFyaz1ibWFw")

        // Create tile data source, map layer
        let mapView = NTMapView()
        let projection = mapView?.getOptions()?.getBaseProjection()
        let styleAsset = NTAssetUtils.loadAsset("busmapstyles-v1.zip")
        let assetPackage = NTZippedAssetPackage(zip: styleAsset)
        
        let vectorTileDataSource = NTCartoOnlineTileDataSource(source: "https://bmap-tiles-a.busmap.vn/data/vietnam.json?lang=vi", debugListener: DebugListener())
        
        let baseLayer = NTCartoVectorTileLayer(dataSource: vectorTileDataSource, styleAssetPackage: assetPackage, styleName: "busmap")
        
//        let baseLayer = NTCartoVectorTileLayer(dataSource: vectorTileDataSource, style: .CARTO_BASEMAP_STYLE_VOYAGER)
        mapView?.getLayers()?.add(baseLayer)

        // Handle map camera
        mapView?.setZoom(12, durationSeconds: 0.3)
        mapView?.setFocus(projection?.fromWgs84(NTMapPos(x: 106.668546, y: 10.798121)), durationSeconds: 0)

        // 2. Create, add marker
        // Initialize an vector data source where to put the elements
        let vectorDataSource = NTLocalVectorDataSource(projection: projection)

        // Initialize a vector layer with the previous data source
        let vectorLayer = NTVectorLayer(dataSource: vectorDataSource)

        // Add the previous vector layer to the map
        mapView?.getLayers()?.add(vectorLayer)

        // Create marker style, add marker
        let markerStyleBuilder = NTMarkerStyleBuilder()
        markerStyleBuilder?.setSize(30)
        markerStyleBuilder?.setColor(NTColor(r: 97, g: 209, b: 91, a: 192))
        let markerStyle = markerStyleBuilder?.buildStyle()

        var pos = projection?.fromWgs84(NTMapPos(x: 106.668546, y: 10.798121))
        let marker = NTMarker(pos: pos, style: markerStyle)

        // Create image marker style
        let imageMarkerStyleBuilder = NTMarkerStyleBuilder()
        imageMarkerStyleBuilder?.setBitmap(NTBitmapUtils.createBitmap(from: UIImage(named: "marker_bus_green")))
        imageMarkerStyleBuilder?.setSize(30)
        let imageMarkerStyle = imageMarkerStyleBuilder?.buildStyle()

        pos = projection?.fromWgs84(NTMapPos(x: 106.669270, y: 10.797973))
        let markerImage = NTMarker(pos: pos, style: imageMarkerStyle)

        vectorDataSource?.add(markerImage)

        self.view = mapView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    @objc func didTapAction() {
        
    }
    
    
}

class DebugListener : NTTileDownloadListener {
    override func onLog(_ message: String!) {
        NSLog(message)
    }
}

extension MapViewController {
    private func makeLabel() -> UILabel {
        let label = UILabel(frame: .zero)
        label.text = "Hello bMap"
        label.textColor = .black
        return label
    }
    
    private func makeButton() -> UIButton {
        let button = UIButton(frame: .zero)
        button.setTitle("Open map", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        return button
    }
}
