//
//  ViewController.swift
//  test
//
//  Created by Nguyễn Văn Rum on 9/28/20.
//

import UIKit
import CartoMobileSDK

class ViewController: UIViewController {

    lazy var label: UILabel = self.makeLabel()
    lazy var button: UIButton = self.makeButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.label)
        self.view.addSubview(self.button)
        self.view.backgroundColor = .white
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.label.frame = CGRect(x: 0, y: 100, width: self.view.bounds.width, height: 50)
        self.button.frame = CGRect(x: self.view.bounds.width / 2 - 50, y: self.label.frame.maxY + 100, width: 100, height: 50)
    }
    
    @objc func didTapAction() {
        let vc = MapViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        NSLog("123 123 123")
    }
}

extension ViewController {
    private func makeLabel() -> UILabel {
        let label = UILabel(frame: .zero)
        label.text = "Hello bMap"
        label.textColor = .black
        return label
    }
    
    private func makeButton() -> UIButton {
        let button = UIButton(frame: .zero)
        button.setTitle("Open map", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(didTapAction), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        button.backgroundColor = .red
        return button
    }
}

