/*
 * Copyright (c) 2016 CartoDB. All rights reserved.
 * Copying and using this code is allowed only according
 * to license terms, as given in https://cartodb.com/terms/
 */

#ifndef _CARTOMOBILESDK_H_
#define _CARTOMOBILESDK_H_


#import <CartoMobileSDK/NTOptions.h>
#import <CartoMobileSDK/NTLayers.h>

#import <CartoMobileSDK/NTAddress.h>
#import <CartoMobileSDK/NTMapBounds.h>
#import <CartoMobileSDK/NTMapEnvelope.h>
#import <CartoMobileSDK/NTMapPos.h>
#import <CartoMobileSDK/NTScreenPos.h>
#import <CartoMobileSDK/NTScreenBounds.h>
#import <CartoMobileSDK/NTMapRange.h>
#import <CartoMobileSDK/NTMapTile.h>
#import <CartoMobileSDK/NTMapVec.h>
#import <CartoMobileSDK/NTTileData.h>
#import <CartoMobileSDK/NTVariant.h>
#import <CartoMobileSDK/NTVariantArrayBuilder.h>
#import <CartoMobileSDK/NTVariantObjectBuilder.h>

#import <CartoMobileSDK/NTAssetTileDataSource.h>
#import <CartoMobileSDK/NTCombinedTileDataSource.h>
#import <CartoMobileSDK/NTOrderedTileDataSource.h>
#import <CartoMobileSDK/NTMergedMBVTTileDataSource.h>
#import <CartoMobileSDK/NTBitmapOverlayRasterTileDataSource.h>
#import <CartoMobileSDK/NTHTTPTileDataSource.h>
#import <CartoMobileSDK/NTMemoryCacheTileDataSource.h>
#import <CartoMobileSDK/NTPersistentCacheTileDataSource.h>
#import <CartoMobileSDK/NTCartoOnlineTileDataSource.h>
#import <CartoMobileSDK/NTMapTilerOnlineTileDataSource.h>
#import <CartoMobileSDK/NTLocalVectorDataSource.h>
#import <CartoMobileSDK/NTTileDownloadListener.h>

#import <CartoMobileSDK/NTFeature.h>
#import <CartoMobileSDK/NTFeatureCollection.h>
#import <CartoMobileSDK/NTLineGeometry.h>
#import <CartoMobileSDK/NTPointGeometry.h>
#import <CartoMobileSDK/NTPolygonGeometry.h>
#import <CartoMobileSDK/NTMultiGeometry.h>
#import <CartoMobileSDK/NTMultiLineGeometry.h>
#import <CartoMobileSDK/NTMultiPointGeometry.h>
#import <CartoMobileSDK/NTMultiPolygonGeometry.h>
#import <CartoMobileSDK/NTGeometrySimplifier.h>
#import <CartoMobileSDK/NTDouglasPeuckerGeometrySimplifier.h>
#import <CartoMobileSDK/NTGeoJSONGeometryReader.h>
#import <CartoMobileSDK/NTGeoJSONGeometryWriter.h>

#import <CartoMobileSDK/NTBitmap.h>
#import <CartoMobileSDK/NTColor.h>
#import <CartoMobileSDK/NTViewState.h>

#import <CartoMobileSDK/NTSolidLayer.h>
#import <CartoMobileSDK/NTRasterTileEventListener.h>
#import <CartoMobileSDK/NTRasterTileLayer.h>
#import <CartoMobileSDK/NTTileLoadListener.h>
#import <CartoMobileSDK/NTUTFGridEventListener.h>
#import <CartoMobileSDK/NTVectorElementEventListener.h>
#import <CartoMobileSDK/NTVectorLayer.h>
#import <CartoMobileSDK/NTVectorTileEventListener.h>
#import <CartoMobileSDK/NTVectorTileLayer.h>
#import <CartoMobileSDK/NTTorqueTileLayer.h>
#import <CartoMobileSDK/NTClusteredVectorLayer.h>
#import <CartoMobileSDK/NTCartoOnlineRasterTileLayer.h>
#import <CartoMobileSDK/NTCartoOnlineVectorTileLayer.h>
#import <CartoMobileSDK/NTClusterElementBuilder.h>

#import <CartoMobileSDK/NTEPSG3857.h>
#import <CartoMobileSDK/NTEPSG4326.h>

#import <CartoMobileSDK/NTCullState.h>

#import <CartoMobileSDK/NTAnimationStyleBuilder.h>
#import <CartoMobileSDK/NTAnimationStyle.h>
#import <CartoMobileSDK/NTBalloonPopupStyleBuilder.h>
#import <CartoMobileSDK/NTBalloonPopupStyle.h>
#import <CartoMobileSDK/NTBalloonPopupButtonStyleBuilder.h>
#import <CartoMobileSDK/NTBalloonPopupButtonStyle.h>
#import <CartoMobileSDK/NTLabelStyleBuilder.h>
#import <CartoMobileSDK/NTLabelStyle.h>
#import <CartoMobileSDK/NTLineStyleBuilder.h>
#import <CartoMobileSDK/NTLineStyle.h>
#import <CartoMobileSDK/NTMarkerStyleBuilder.h>
#import <CartoMobileSDK/NTMarkerStyle.h>
#import <CartoMobileSDK/NTPointStyleBuilder.h>
#import <CartoMobileSDK/NTPointStyle.h>
#import <CartoMobileSDK/NTPolygon3DStyleBuilder.h>
#import <CartoMobileSDK/NTPolygon3DStyle.h>
#import <CartoMobileSDK/NTPolygonStyleBuilder.h>
#import <CartoMobileSDK/NTPolygonStyle.h>
#import <CartoMobileSDK/NTPopupStyleBuilder.h>
#import <CartoMobileSDK/NTPopupStyle.h>
#import <CartoMobileSDK/NTTextStyleBuilder.h>
#import <CartoMobileSDK/NTTextStyle.h>
#import <CartoMobileSDK/NTGeometryCollectionStyle.h>
#import <CartoMobileSDK/NTGeometryCollectionStyleBuilder.h>

#import <CartoMobileSDK/NTMapRenderer.h>
#import <CartoMobileSDK/NTMapRendererListener.h>
#import <CartoMobileSDK/NTRendererCaptureListener.h>

#import <CartoMobileSDK/ui/MapView.h>
#import <CartoMobileSDK/NTMapClickInfo.h>
#import <CartoMobileSDK/NTMapEventListener.h>
#import <CartoMobileSDK/NTBalloonPopupButtonClickInfo.h>
#import <CartoMobileSDK/NTRasterTileClickInfo.h>
#import <CartoMobileSDK/NTVectorTileClickInfo.h>
#import <CartoMobileSDK/NTVectorElementClickInfo.h>

#import <CartoMobileSDK/NTAssetUtils.h>
#import <CartoMobileSDK/NTBitmapUtils.h>
#import <CartoMobileSDK/NTTileUtils.h>
#import <CartoMobileSDK/NTLog.h>
#import <CartoMobileSDK/NTLogEventListener.h>
#import <CartoMobileSDK/utils/ExceptionWrapper.h>

#import <CartoMobileSDK/NTBalloonPopup.h>
#import <CartoMobileSDK/NTBalloonPopupButton.h>
#import <CartoMobileSDK/NTBalloonPopupEventListener.h>
#import <CartoMobileSDK/NTCustomPopup.h>
#import <CartoMobileSDK/NTCustomPopupHandler.h>
#import <CartoMobileSDK/NTGeometryCollection.h>
#import <CartoMobileSDK/NTLabel.h>
#import <CartoMobileSDK/NTLine.h>
#import <CartoMobileSDK/NTMarker.h>
#import <CartoMobileSDK/NTPoint.h>
#import <CartoMobileSDK/NTPolygon3D.h>
#import <CartoMobileSDK/NTPolygon.h>
#import <CartoMobileSDK/NTPopup.h>
#import <CartoMobileSDK/NTText.h>

#import <CartoMobileSDK/NTAssetPackage.h>
#import <CartoMobileSDK/NTZippedAssetPackage.h>
#import <CartoMobileSDK/NTCompiledStyleSet.h>
#import <CartoMobileSDK/NTCartoCSSStyleSet.h>
#import <CartoMobileSDK/NTVectorTileDecoder.h>
#import <CartoMobileSDK/NTCartoVectorTileDecoder.h>
#import <CartoMobileSDK/NTMBVectorTileDecoder.h>
#import <CartoMobileSDK/NTTorqueTileDecoder.h>
#import <CartoMobileSDK/NTVectorTileFeature.h>
#import <CartoMobileSDK/NTVectorTileFeatureCollection.h>

#ifdef _CARTO_EDITABLE_SUPPORT
#import <CartoMobileSDK/NTEditableVectorLayer.h>
#import <CartoMobileSDK/NTVectorEditEventListener.h>
#endif

#endif
