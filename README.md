# Demo Cloudflare - Carto
## Run
- Navigation to project, install CoacaPod dependencies:
`pod install`
- Lauch project with Xcode 
## Note
This is project demo case crash iOS when using Cloudflare Mobile SDK with CartoDB Mobile SDK

[[Open] Issue open in Cloudflare Community](https://community.cloudflare.com/t/crash-ios-issue-when-using-cloudflare-mobile-sdk-with-carto-mobile-sdk-map-sdk/225273)

[[Closed] Issue open in CartoDB Github](https://github.com/CartoDB/mobile-sdk/issues/400)

## Crash issue
### Condition procedure crash
- Cloudflare version 5.0.0.2: https://github.com/CocoaPods/Specs/tree/master/Specs/6/9/b/CloudflareMobileSDK/5.0.0.2
- iOS < 14
- Real device
- Build in release mode
- Not plugged or attached to device (Run app via Xcode). Maybe it seems like https://stackoverflow.com/questions/9931439/ios-crash-only-when-not-running-via-xcode-concidence

### Reprocedure crash
Open app, click button "Open map". At map screen, move map to load. Crash occurs when moving.

## Screenshots
### Main Screen

![Main](https://i.imgur.com/pHJKDFn.png)

### Map Screen

![Map](https://i.imgur.com/7ouT8NE.png)

